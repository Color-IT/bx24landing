'use strict';

// Подключаем плагин require-dir
const
	del  = require('del'),
	gulp = require('gulp'),
	rDir = require('require-dir');


// Подключаем к файлу все js-файлы с тасками из папки gulptasks
rDir('./gulptasks');


// Собираем проект для разработки
gulp.task('build:dev', ['styles', 'scripts', 'pages', 'assets']);

// Собираем проект для продакшена
gulp.task('build:product', ['product:images','product:styles','product:scripts','product:static']);


// Команда по умолчанию. Выполняет сборку и запускает слежение за файлами
gulp.task('default', ['build:dev', 'watch']);


// Сборка, отслеживание изменений и запуск сервера
gulp.task('serve'  , ['build:dev', 'watch', 'server']);


// Очистка директории от сборок
gulp.task('clean', () => { del(['./public/**', './www/**']) });