// Отслеживание изменений

const gulp = require('gulp');


gulp.task('watch', () => {

  // Следим за стилями. При изменении запускаем общий таск для стилей - styles
  gulp.watch('./source/**/*.styl', ['styles']);

  // Следим за страницами. При изменении запускаем общий таск для страниц - pages
  gulp.watch('./source/**/*.pug', ['pages']);

  // Следим за общими скриптами проекта. При изменении запускаем таск для польз. скриптов
  gulp.watch(['./source/base/*.js', './source/blocks/**/*.js'], ['scripts:user']);

});
