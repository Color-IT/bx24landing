const
    gulp     = require('gulp'),
    csso     = require('gulp-csso'),
    uglify   = require('gulp-uglifyjs'),
    stylus   = require('gulp-stylus'),
    concat   = require('gulp-concat'),
    plumber  = require('gulp-plumber'),
    imagemin = require('gulp-imagemin');


const postProcessors = [ csso ];


gulp.task('product:styles', () => {

  return gulp
    .src(['./public/css/libs.css', './public/css/styles.css']) 
    .pipe( concat('styles.min.css') )
    .pipe( csso() )
    .pipe( gulp.dest('./www/css/') );

});



gulp.task('product:scripts', () => {

  gulp
    .src('./public/js/scripts.js')
    .pipe( uglify() )
    .pipe( gulp.dest('./www/js/') );

  return gulp
    .src('./public/js/libs.js')
    .pipe( uglify() )
    .pipe( gulp.dest('./www/js/') );

});



gulp.task('product:images', () => {

  return gulp
    .src('./public/img/**/*')
    .pipe(imagemin())
    .pipe(gulp.dest('./www/img'));

});


gulp.task('product:static', () => {

  gulp
    .src('./public/css/custom.css').pipe( gulp.dest('./www/css/') );

  return gulp
    .src('./public/*.html').pipe( gulp.dest('./www/') );

});