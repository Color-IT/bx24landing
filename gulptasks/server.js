// Локальный сервер

const
	gulp   = require('gulp'),
	server = require('browser-sync').create(),
	reload = require('browser-sync').reload;


// Запускаем BrpwserSync по адресу localhost:3000
// Адрес для тестирования на других устройствах локальной сети будет указан в консоли
gulp.task('server', () => {

	// Инициализация сервера и запуск в папке сборки
	server.init({
	    server: { baseDir: "./public/" }
	});

	return gulp
	    .watch('./public/**/*') // Следим за изменениями файлов в папке сборки...
	    .on('change', reload);  // ...и обновляем страницу в браузере при внесении изменений

});
