// Работа с шаблонами страниц
const
	gulp       = require('gulp'),
	pug        = require('gulp-pug'),
	plumber    = require('gulp-plumber'),
	htmlpretty = require('gulp-html-prettify');


// Переводим PUG в HTML
gulp.task('pages:transpile', () => {

  return gulp
    .src('./source/pages/*.pug')  	// Подаем на вход файлы страниц *.pug
    .pipe( plumber() )				// Запускаем отслеживание ошибок
    .pipe( pug({pretty: true}) )	// Транспайлим PUG в HTML
    .pipe( htmlpretty() )			// Наводим порядок в полученном HTML-коде
    .pipe( gulp.dest('./public/') ) // Сохраняем страницы в корне папки билда

});


// Main task
gulp.task('pages', ['pages:transpile']);
