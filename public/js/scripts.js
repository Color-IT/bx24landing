// Your scripts goes here
// .whoweare scripts goes here
// .button scripts goes here
var buttons = [].slice.call(document.querySelectorAll('div.button'));

buttons.forEach(function (el) {

  el.onclick = showModalForm;
});
// .calltoaction scripts goes here
// .cases scripts goes here
var casesBlock = document.querySelector('.cases');
var caseItems = [].slice.call(document.querySelectorAll('.cases__item'));

caseItems.forEach(function (el, arr, i) {

  var clicker = el.querySelector('.cases__info span');

  clicker.onclick = function (e) {
    if (this.innerHTML != 'Закрыть') {

      el.classList.add('zz-top');
      el.querySelector('.cases__info').classList.add('active');
      this.innerHTML = 'Закрыть';
      casesBlock.classList.add('one-opened');
    } else {
      el.classList.remove('zz-top');
      el.querySelector('.cases__info').classList.remove('active');
      this.innerHTML = 'Подробнее';
      casesBlock.classList.remove('one-opened');
    }
  };
});
// .header scripts goes here
// .modal scripts goes here
var modal = document.querySelector('.modal');
var modalStatus = document.querySelector('.modal__status');

function showModalForm() {
  modal.classList.add('modal--active');
}

function closeModalForm() {
  modal.classList.remove('modal--active');
}

document.querySelector('.modal__splash').onclick = closeModalForm;

function showModalInfo(status, text) {

  closeModalForm();

  modalStatus.classList.add('modal__status--' + status);

  var statusText = text || 'Успешно';

  modalStatus.innerHTML = statusText;

  setTimeout(function () {
    modalStatus.classList.remove('modal__status--' + status);
  }, 5000);
}
// .portfolio scripts goes here
// .steps scripts goes here
// .title scripts goes here
// .why scripts goes here
//# sourceMappingURL=.maps/scripts.js.map
