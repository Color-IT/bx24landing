// .cases scripts goes here
var casesBlock = document.querySelector('.cases');
var caseItems = [].slice.call( document.querySelectorAll('.cases__item') );

caseItems.forEach( function(el, arr, i) {

  var clicker = el.querySelector('.cases__info span');

  clicker.onclick = function(e) {
    if (this.innerHTML != 'Закрыть') {

      el.classList.add('zz-top');
      el.querySelector('.cases__info').classList.add('active');
      this.innerHTML = 'Закрыть';
      casesBlock.classList.add('one-opened');
    }

    else {
      el.classList.remove('zz-top');
      el.querySelector('.cases__info').classList.remove('active');
      this.innerHTML = 'Подробнее';
      casesBlock.classList.remove('one-opened');
    }
  }

});