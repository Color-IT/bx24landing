// .modal scripts goes here
var modal = document.querySelector('.modal');
var modalStatus = document.querySelector('.modal__status');

function showModalForm() {
  modal.classList.add('modal--active');
}


function closeModalForm() {
  modal.classList.remove('modal--active');
}

document.querySelector('.modal__splash').onclick = closeModalForm;


function showModalInfo(status, text) {

  closeModalForm() 

  modalStatus.classList.add('modal__status--' + status)

  var statusText = text || 'Успешно';

  modalStatus.innerHTML = statusText;

  setTimeout(function() {
    modalStatus.classList.remove('modal__status--' + status)
  }, 5000);

}